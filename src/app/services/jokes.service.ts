import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class JokesService {
  constructor(private http: HttpClient) {}
  jokeUrl: string = "https://icanhazdadjoke.com";
  jsonHeader: object = {
    headers: {
      Accept: "application/json",
    },
  };
  picsumUrl: string = "https://picsum.photos/id/";

  getRandomJoke() {
    return this.http.get(this.jokeUrl, this.jsonHeader);
  }

  getRandomPic(id) {
    const picsumUrlInfo: string = `${this.picsumUrl}${id}/info`;
    return new Promise<any>((resolve, reject) => {
      this.http
        .get(picsumUrlInfo, this.jsonHeader)
        .subscribe((response) => resolve(response), error => reject(error));
    });
  }
}
