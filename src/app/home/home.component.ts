import { Component, OnInit, ElementRef } from '@angular/core';
import { JokesService } from '../services/jokes.service';
import { interval } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  config: any;
  fullpage_api: any;

  sections = [];
  numberOfSections = [...Array(7).keys()];
  showSections: boolean = false;

  section: object = {
    joke: '',
    photo: '',
  };

  timer;
  owner: String = 'barnedorenz@gmail.com';
  emojis: String = '👌👋💯🤡😂🤣';
  hint: String = '(scroll down)';

  constructor(private service: JokesService, private elementRef: ElementRef) {
    this.config = {
      menu: '#menu',
      controlArrows: false,
      verticalCentered: true,
    };
  }

  ngOnInit(): void {
    this.checkIfFirstVisit();
    this.setTimer();
  }

  setTimer() {
    const MS_IN_A_DAY = 86400000;
    const MS_IN_AN_HOUR = 3600000;
    const MS_IN_A_MINUTE = 60000;
    const MS_IN_A_SECOND = 1000;

    interval(1000).subscribe((count) => {
      let dateForNewData = JSON.parse(localStorage.getItem('dateForNewData'));
      let todayInMilliseconds: any = new Date().getTime();

      if (dateForNewData != null && todayInMilliseconds > dateForNewData) {
        this.sections = [];
        this.showSections = false;
        localStorage.clear();
        this.checkIfFirstVisit();
      }

      dateForNewData = new Date(dateForNewData);
      todayInMilliseconds = new Date(todayInMilliseconds);

      const MS_DIFFERENCE = dateForNewData - todayInMilliseconds;
      const diffHrs = Math.floor((MS_DIFFERENCE % MS_IN_A_DAY) / MS_IN_AN_HOUR); // hours
      const diffMins = Math.floor(
        ((MS_DIFFERENCE % MS_IN_A_DAY) % MS_IN_AN_HOUR) / MS_IN_A_MINUTE
      );
      const diffSecs = Math.floor(
        (((MS_DIFFERENCE % MS_IN_A_DAY) % MS_IN_AN_HOUR) % MS_IN_A_MINUTE) /
          MS_IN_A_SECOND
      );

      if (diffHrs >= 0) {
        this.timer = `${diffHrs} hrs, ${diffMins} mins, ${
          diffSecs < 10 ? `0${diffSecs}` : diffSecs
        } secs`;
      }
    });
  }

  checkIfFirstVisit() {
    const sections = JSON.parse(localStorage.getItem('sections'));
    sections ? this.revisitOnly(sections) : this.setSections();
  }

  revisitOnly(sections) {
    this.sections = sections;
    this.showSections = true;
  }

  setSections() {
    this.numberOfSections.forEach((item, index) => {
      if (index === 1) {
        this.setFirstSlide();
        return;
      }
      this.getRandomJoke();
    });
  }

  getRandomJoke() {
    this.service.getRandomJoke().subscribe((response: any) => {
      const joke = response.joke;
      this.setJokeWithPhoto(joke);
    });
  }

  setJokeWithPhoto(joke) {
    const randomNumber = this.getRandomNumber(0, 1100);
    this.service.getRandomPic(randomNumber).then(
      (response) => {
        const photo = `${response.download_url}?blur=2`;

        const section = {
          joke,
          photo,
        };
        this.sections.push(section);
        this.finishSections();
      },
      (error) => {
        if (!error.ok) {
          this.setJokeWithPhoto(joke);
        }
      }
    );
  }

  setLastSlide() {
    const notifyMessage = `Come back after this for more 🔥 jokes!`;
    this.setJokeWithPhoto(notifyMessage);
  }

  setFirstSlide() {
    const title = `Jokes everyday! LOL`;
    this.setJokeWithPhoto(title);
  }

  finishSections() {
    if (this.sections.length === this.numberOfSections.length) {
      this.setLastSlide();
    } else if (this.sections.length > this.numberOfSections.length) {
      this.setLocalStorage();
      this.showSections = true;
    }
  }

  getRef(fullPageRef) {
    this.fullpage_api = fullPageRef;
  }

  setLocalStorage() {
    const JOKES_AS_JSON = JSON.stringify(this.sections);
    localStorage.setItem('sections', JOKES_AS_JSON);

    const ADD_ONE_DAY = new Date();
    ADD_ONE_DAY.setDate(ADD_ONE_DAY.getDate() + 1);

    const DATE_FOR_NEW_DATA = JSON.stringify(ADD_ONE_DAY.getTime());
    localStorage.setItem('dateForNewData', DATE_FOR_NEW_DATA);
  }

  getRandomNumber(min, max) {
    return Math.ceil(Math.random() * (max - min) + min);
  }
}
